package com.ruzzz.lib.display
{
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.geom.Rectangle;

    public final class McUtils
    {
        public function McUtils()
        {
            throw new Error("This is for using with static methods only");
        }

        static public function checkFramesRange(mc: MovieClip, startFrame: int, endFrame: int): int
        {
            if (endFrame == -1)
                endFrame = mc.totalFrames;
            else if (startFrame > endFrame)
                throw new ArgumentError("endFrame too small");

            if (startFrame > endFrame)
                throw new ArgumentError("startFrame too big");

            return endFrame;
        }

        static public function nestedCheckFramesRange(mc: MovieClip, startFrame: int, endFrame: int): int
        {
            if (endFrame == -1)
                endFrame = nestedNumFrames(mc);
            else if (startFrame > endFrame)
                throw new ArgumentError("endFrame too small");

            if (startFrame > endFrame)
                throw new ArgumentError("startFrame too big");

            return endFrame;
        }

        static public function calcBounds(mc: MovieClip,
                                          startFrame: int = 1,
                                          endFrame: int = -1,
                                          targetCoordinateSpace: DisplayObject = null): Rectangle
        {
            endFrame = checkFramesRange(mc, startFrame, endFrame);
            if (!targetCoordinateSpace)
                targetCoordinateSpace = mc;
            var rc: Rectangle = mc.getBounds(targetCoordinateSpace);
            for (var i: int = startFrame + 1; i <= endFrame; ++i) {
                nextFrame(mc);
                rc = rc.union(mc.getBounds(targetCoordinateSpace));
            }
            return rc;
        }

        static public function nestedCalcBounds(mc: MovieClip,
                                                startFrame: int = 1,
                                                endFrame: int = -1,
                                                targetCoordinateSpace: DisplayObject = null): Rectangle
        {
            endFrame = nestedCheckFramesRange(mc, startFrame, endFrame);
            if (!targetCoordinateSpace)
                targetCoordinateSpace = mc;
            var rc: Rectangle = mc.getBounds(targetCoordinateSpace);
            for (var i: int = startFrame + 1; i <= endFrame; ++i) {
                nestedNextFrame(mc);
                rc = rc.union(mc.getBounds(targetCoordinateSpace));
            }
            return rc;
        }

        static public function nextFrame(mc: MovieClip): void
        {
            var t: int = mc.totalFrames;
            if (t > 1) {
                var i: int = mc.currentFrame;
                if (i >= t)
                    i = 1;
                else
                    ++i;
                mc.gotoAndStop(i);
            }
        }

        static public function nestedNextFrame(source: DisplayObjectContainer): void
        {
            if (source is MovieClip)
                nextFrame(source as MovieClip);
            var i: int = source.numChildren;
            while (--i > -1) {
                var child: DisplayObject = source.getChildAt(i);
                if (child is MovieClip)
                    nextFrame(child as MovieClip);
                if (child is DisplayObjectContainer)
                    nestedNextFrame(child as DisplayObjectContainer);
            }
        }

        static public function nestedNumFrames(source: DisplayObjectContainer): int
        {
            var result: int = source is MovieClip ? (source as MovieClip).totalFrames : 1;
            var i: int = source.numChildren;
            while (--i > -1) {
                var child: DisplayObject = source.getChildAt(i);
                if (child is MovieClip)
                    result = Math.max(result, (child as MovieClip).totalFrames);
                if (child is DisplayObjectContainer)
                    result = Math.max(result, nestedNumFrames(child as DisplayObjectContainer));
            }
            return result;
        }

        static public function nestedSetFrame(source: DisplayObjectContainer, frame: int): void
        {
            if (source is MovieClip)
                (source as MovieClip).gotoAndStop(frame);
            var i: int = source.numChildren;
            while (--i > -1) {
                var child: DisplayObject = source.getChildAt(i);
                if (child is MovieClip)
                    (child as MovieClip).gotoAndStop(frame);
                if (child is DisplayObjectContainer)
                    nestedSetFrame(child as DisplayObjectContainer, frame);
            }
        }
    }
}