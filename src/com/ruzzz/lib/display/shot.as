package com.ruzzz.lib.display
{
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.geom.Matrix;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    public function shot(source: DisplayObject, scale: Point, clipRect: Rectangle): ShotResult
    {
        var matrix: Matrix = new Matrix;
        matrix.scale(scale.x, scale.y);
        var newSize: Point = matrix.transformPoint(clipRect.size);

        var pivotOffset: Point = clipRect.topLeft;
        pivotOffset = matrix.transformPoint(pivotOffset);
        matrix.translate(-pivotOffset.x, -pivotOffset.y);

        var rc: Rectangle = new Rectangle();
        rc.width = Math.floor(newSize.x);
        rc.height = Math.floor(newSize.y);
        var bmp: BitmapData = new BitmapData(rc.width, rc.height, true, 0);
        bmp.draw(source, matrix, null, null, rc, true);
        return new ShotResult(bmp, pivotOffset);
    }
}
