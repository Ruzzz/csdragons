package com.ruzzz.lib.display
{
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.geom.Point;

    public final class ShotResult
    {
        public var shot: BitmapData;
        public var pivotOffset: Point;

        public function ShotResult(shot: BitmapData, pivotOffset: Point = null)
        {
            this.shot = shot;
            this.pivotOffset = pivotOffset;
        }

        public function applyOffset(obj: DisplayObject): void
        {
            if (pivotOffset) {
                obj.x += pivotOffset.x;
                obj.y += pivotOffset.y;
            }
        }
    }
}
