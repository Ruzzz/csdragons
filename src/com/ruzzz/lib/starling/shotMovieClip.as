package com.ruzzz.lib.starling
{
    import com.ruzzz.lib.display.McUtils;

    import flash.display.MovieClip;
    import flash.geom.Matrix;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import starling.textures.Texture;

    public function shotMovieClip(source: MovieClip, scale: Point, clipRect: Rectangle,
                                  startFrame: int = 1, endFrame: int = -1, nested: Boolean = true): TexturesData
    {
        // TODO: Note: Возможно после вызова нужно будет вызвать mc.gotoAndStop(1) или McUtils.nestedSetFrame(mc, 1)

        endFrame = nested
                ? McUtils.nestedCheckFramesRange(source, startFrame, endFrame)
                : McUtils.checkFramesRange(source, startFrame, endFrame);

        var pivotOffset: Point = clipRect.topLeft;
        var matrix: Matrix = new Matrix;
        matrix.scale(scale.x, scale.y);
        pivotOffset = matrix.transformPoint(pivotOffset);

        if (nested)
            McUtils.nestedSetFrame(source, startFrame);
        else
            source.gotoAndStop(startFrame);

        // scan frames

        var textures: Vector.<Texture> = new Vector.<Texture>();
        for (var i: int = startFrame; i <= endFrame; ++i) {
            var td: TextureData = shotTexture(source, scale, clipRect);
            textures.push(td.texture);

            if (nested)
                McUtils.nestedNextFrame(source);
            else
                McUtils.nextFrame(source);
        }

        return new TexturesData(textures, pivotOffset);
    }
}
