package com.ruzzz.lib.starling
{
    import flash.geom.Point;

    import starling.display.DisplayObject;

    public class PositionableData
    {
        public var pivotOffset: Point;

        public function PositionableData(coords: Point = null)
        {
            this.pivotOffset = coords;
        }

        public function applyOffset(obj: DisplayObject): void
        {
            if (pivotOffset) {
                obj.x += pivotOffset.x;
                obj.y += pivotOffset.y;
            }
        }
    }
}