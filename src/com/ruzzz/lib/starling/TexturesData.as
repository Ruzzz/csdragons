package com.ruzzz.lib.starling
{
    import com.ruzzz.lib.IDisposable;

    import flash.geom.Point;

    import starling.textures.Texture;

    public final class TexturesData extends PositionableData implements IDisposable
    {
        public var textures: Vector.<Texture>;

        public function TexturesData(textures: Vector.<Texture>, pivotOffset: Point = null)
        {
            super(pivotOffset);
            this.textures = textures;
        }
        
        public function dispose(): void
        {
            for each (var texture: Texture in textures)
                texture.dispose();
            textures = null;
            pivotOffset = null;
        }
    }
}
