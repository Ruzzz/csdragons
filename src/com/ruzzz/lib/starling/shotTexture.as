package com.ruzzz.lib.starling
{
    import com.ruzzz.lib.display.ShotResult;
    import com.ruzzz.lib.display.shot;

    import flash.display.DisplayObject;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    public function shotTexture(source: DisplayObject, scale: Point, clipRect: Rectangle): TextureData
    {
        var d: ShotResult = shot(source, scale, clipRect);
        var ret: TextureData = TextureData.fromShotData(d);
        d.shot.dispose();
        return ret;
    }
}
