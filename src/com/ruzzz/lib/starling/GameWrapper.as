package com.ruzzz.lib.starling
{
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;

    import starling.core.Starling;
    import starling.utils.HAlign;
    import starling.utils.VAlign;

    public class GameWrapper extends Sprite
    {
        public var starling_: Starling;

        private var rootClass_: Class;
        private var softwareRenderFrameRate_: Number;
        private var starlingErrorChecking_: Boolean;
        private var starlingShowStats_: Boolean;

        public function GameWrapper(rootClass: Class,
                                    softwareRenderFrameRate: Number = 30,
                                    starlingErrorChecking: Boolean = false,
                                    showStats: Boolean = false)
        {
            rootClass_ = rootClass;
            softwareRenderFrameRate_ = softwareRenderFrameRate;
            starlingErrorChecking_ = starlingErrorChecking;
            starlingShowStats_ = showStats;

            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        private function addedToStageHandler(e: Event): void
        {
            removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);

            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, context3DCreateHandler);

            starling_ = new Starling(rootClass_, stage);
            starling_.enableErrorChecking = starlingErrorChecking_;
            starling_.start();
            if (starlingShowStats_)
                starling_.showStatsAt(HAlign.LEFT, VAlign.TOP, 2);
        }

        private function context3DCreateHandler(event: Event): void
        {
            stage.stage3Ds[0].removeEventListener(Event.CONTEXT3D_CREATE, context3DCreateHandler);
            if (Starling.context.driverInfo.toLowerCase().indexOf("software") != -1)
                Starling.current.nativeStage.frameRate = softwareRenderFrameRate_;
        }
    }
}
