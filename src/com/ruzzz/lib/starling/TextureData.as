package com.ruzzz.lib.starling
{
    import com.ruzzz.lib.IDisposable;
    import com.ruzzz.lib.display.ShotResult;

    import flash.geom.Point;

    import starling.textures.Texture;

    public final class TextureData extends PositionableData implements IDisposable
    {
        public var texture: Texture;

        public function TextureData(texture: Texture, pivotOffset: Point = null)
        {
            super(pivotOffset);
            this.texture = texture;
        }

        public function dispose(): void
        {
            texture.dispose();
            texture = null;
            pivotOffset = null;
        }

        static public function fromShotData(d: ShotResult): TextureData
        {
            var po: Point = new Point(Math.round(d.pivotOffset.x), Math.round(d.pivotOffset.y));
            return new TextureData(Texture.fromBitmapData(d.shot), po);
        }
    }
}
