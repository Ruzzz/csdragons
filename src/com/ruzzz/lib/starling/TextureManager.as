package com.ruzzz.lib.starling
{
    import com.ruzzz.lib.IDisposable;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    public final class TextureManager
    {
        private var map: Object;  // [name] = TextureData or TexturesData

        public function TextureManager()
        {
            clear();
        }

        public function clear(): void
        {
            map = {};
        }

        public function update(name: String,
                               source: DisplayObject, scale: Point, clipRect: Rectangle): TextureData
        {
            remove(name);
            var d: TextureData = shotTexture(source, scale, clipRect);
            map[name] = d;
            return d;
        }

        public function updateMc(name: String,
                                 source: MovieClip, scale: Point, clipRect: Rectangle,
                                 startFrame: int = 1,
                                 endFrame: int = -1): TexturesData
        {
            remove(name);
            var d: TexturesData = shotMovieClip(source, scale, clipRect, startFrame, endFrame);
            map[name] = d;
            return d;
        }

        public function texture(name: String): TextureData
        {
            return map[name];
        }

        public function textures(name: String): TexturesData
        {
            return map[name];
        }

        public function remove(name: String): void
        {
            if (exists(name)) {
                var d: IDisposable = map[name];
                d.dispose();
                delete map[name];
            }
        }

        private function exists(name: String): Boolean
        {
            return map[name] != null;
        }
    }
}
