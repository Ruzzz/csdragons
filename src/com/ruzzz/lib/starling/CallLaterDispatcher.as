package com.ruzzz.lib.starling
{
    import starling.display.DisplayObject;
    import starling.events.EnterFrameEvent;

    public final class CallLaterDispatcher
    {
        // TODO Review this class

        private var target: DisplayObject = null;
        private var methods: Vector.<Function> = null;
        private var args: Vector.<Array> = null;

        private var len: int = 0;

        public function CallLaterDispatcher(target: DisplayObject, numMethodsLimit: int)
        {
            this.target = target;
            this.methods = new Vector.<Function>(numMethodsLimit, true);
            this.args = new Vector.<Array>(numMethodsLimit, true);
        }

        public function queue(method: Function, ...args): void
        {
            var index: int = methods.indexOf(method);
            if (index == -1) {
                index = len;
                ++len;
                methods[index] = method;
            }
            args[index] = args;
            if (!target.hasEventListener(EnterFrameEvent.ENTER_FRAME))
                target.addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
        }

        public function runDeferred(): void
        {
            if (target.hasEventListener(EnterFrameEvent.ENTER_FRAME))
                target.removeEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);

            for (var i: int = 0; i < len; ++i) {
                var m: Function = methods[i];
                var a: Array = args[i];
                if (a is Array)
                    m.apply(null, a);
                else
                    m();

                methods[i] = null;
                args[i] = null;
            }

            len = 0;
        }

        // EnterFrameEvent.ENTER_FRAME event dispatch
        // on object that is connected to the stage only
        private function enterFrameHandler(e: EnterFrameEvent): void
        {
            runDeferred();
        }
    }
}
