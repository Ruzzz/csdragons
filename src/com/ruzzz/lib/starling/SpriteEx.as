package com.ruzzz.lib.starling
{
    import starling.display.DisplayObject;
    import starling.display.Sprite;
    import starling.events.Event;

    public class SpriteEx extends Sprite
    {
        protected var callLaterDispatcher: CallLaterDispatcher = null;

        private var inited_: Boolean;
        protected function get inited(): Boolean
        {
            return inited_;
        }

        public function SpriteEx(numCallLaterMethods: int = 5)
        {
            super();
            this.callLaterDispatcher = new CallLaterDispatcher(this as DisplayObject, numCallLaterMethods);
            //callLaterDispatcher.queue(updateSize);  // TODO Need?
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        protected var width_: int = 0;
        override public function get width(): Number
        {
            return width_;
        }
        override public function set width(value: Number): void
        {
            var newValue: int = value;
            if (width_ != newValue) {
                width_ = newValue;
                callLaterDispatcher.queue(updateSize);
                //updateSize();
            }
        }

        public function get $width(): Number
        {
            return super.width;
        }
        public function set $width(value: Number): void
        {
            super.width = value;
        }

        protected var height_: int = 0;
        override public function get height(): Number
        {
            return height_;
        }
        override public function set height(value: Number): void
        {
            var newValue: int = value;
            if (height_ != newValue) {
                height_ = newValue;
                callLaterDispatcher.queue(updateSize);
                //updateSize();
            }
        }

        public function get $height(): Number
        {
            return super.height;
        }
        public function set $height(value: Number): void
        {
            super.height = value;
        }

        public function removeAllChildren(dispose: Boolean = true): void
        {
            removeChildren(0, -1, dispose);
        }

        private function addedToStageHandler(e: Event): void
        {
            inited_ = true;
            init();
            callLaterDispatcher.runDeferred();
            addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
        }

        private function removedFromStageHandler(e: Event): void
        {
            inited_ = false;
            freeUp();
        }

        protected function init(): void
        {
        }

        protected function updateSize(): void
        {
        }

        protected function freeUp(): void
        {
        }

        override public function dispose(): void
        {
            inited_ = false;
            super.dispose();
        }
    }
}
