package com.ruzzz.lib
{
    public interface IDisposable
    {
        function dispose(): void;
    }
}
