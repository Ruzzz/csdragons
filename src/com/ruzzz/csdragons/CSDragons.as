package com.ruzzz.csdragons
{
    import com.ruzzz.lib.starling.GameWrapper;

    import com.ruzzz.csdragons.view.AppSprite;

    [SWF(width="400", height="600", frameRate="60", backgroundColor="0")]
    public class CSDragons extends GameWrapper
    {
        public function CSDragons()
        {
            super(AppSprite, 60, true, true);
        }
    }
}
