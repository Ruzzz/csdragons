package com.ruzzz.csdragons.model.events
{
    import flash.events.Event;

    public final class FatalErrorEvent extends Event
    {
        //    types
        public static const UNCAUGHT_ERROR: String = "uncaughtError";
        public static const FATAL_ERROR: String = "fatalError";

        public var message: String = null;

        public function FatalErrorEvent(type: String, message: String)
        {
            super(type);
            this.message = message;
        }

        override public function clone(): Event
        {
            return new FatalErrorEvent(type, message);
        }
    }
}
