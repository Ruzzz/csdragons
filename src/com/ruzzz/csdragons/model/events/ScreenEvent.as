package com.ruzzz.csdragons.model.events
{
	import flash.events.Event;

	public final class ScreenEvent extends Event
	{
		static public const SHOW: String = "show";
		static public const PREVIOUS: String = "previous";

		public var data: * = null;

		public function ScreenEvent(type: String, data: * = null)
		{
			super(type);
			this.data = data;
		}

		public function get name(): String
		{
			// For SHOW
			return data as String;
		}

		override public function clone(): Event
		{
			return new ScreenEvent(type, data);
		}
	}
}
