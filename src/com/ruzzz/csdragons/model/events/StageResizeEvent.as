package com.ruzzz.csdragons.model.events
{
	import flash.events.Event;

	public final class StageResizeEvent extends Event
	{
		public static const RESIZED: String = "resized";

		public var width: int = 0;
		public var height: int = 0;

		public function StageResizeEvent(type: String, width: int, height: int)
		{
			super(type);
			this.width = width;
			this.height = height;
		}

		override public function clone(): Event
		{
			return new StageResizeEvent(type, width, height);
		}
	}
}