package com.ruzzz.csdragons.view.skin
{
    import com.ruzzz.lib.display.McUtils;
    import com.ruzzz.lib.starling.TexturesData;
    import com.ruzzz.lib.starling.TextureManager;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.utils.Dictionary;

    import library.GreenDragon;
    import library.GreenDragonBounds;
    import library.GreenDragonHide;
    import library.GreenDragonShow;

    public class DragonSkin
    {
        static public const WIDTH_DIV: Number = 5;

        // Names of textures
        static public const NORMAL: String = "norm";
        static public const SHOW: String = "show";
        static public const HIDE: String = "hide";

        static private var ASSETS_MAP: Dictionary = new Dictionary();
        ASSETS_MAP[SHOW] = GreenDragonShow;
        ASSETS_MAP[NORMAL] = GreenDragon;
        ASSETS_MAP[HIDE] = GreenDragonHide;

        private var tm: TextureManager;
        private var width_: Number;

        public function DragonSkin(width: Number)
        {
            width_ = width;
            tm = new TextureManager;
            update(width);
        }

        public function get width(): Number
        {
            return width_;
        }

        public function getTextures(name: String): TexturesData
        {
            return tm.textures(name);
        }

        private function update(width: Number): void
        {
            var bounds: DisplayObject = new GreenDragonBounds();
            for (var txName: String in ASSETS_MAP) {
                var AssetCls: Object = ASSETS_MAP[txName];
                var instance: MovieClip = new AssetCls() as MovieClip;
                var scale: Point = new Point(width / bounds.width, width / bounds.width);
                var rc: Rectangle = McUtils.nestedCalcBounds(instance, 1, -1, instance);
                tm.updateMc(txName, instance, scale, rc);
            }
        }
    }
}
