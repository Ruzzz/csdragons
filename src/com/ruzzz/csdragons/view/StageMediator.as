package com.ruzzz.csdragons.view
{
    import com.ruzzz.csdragons.model.events.FatalErrorEvent;
    import com.ruzzz.csdragons.model.events.StageResizeEvent;

    import flash.display.Stage;
    import flash.events.ErrorEvent;
    import flash.events.UncaughtErrorEvent;
    import flash.geom.Rectangle;
    import flash.system.Capabilities;

    import org.robotlegs.mvcs.StarlingMediator;

    import starling.core.Starling;
    import starling.events.ResizeEvent;

    public final class StageMediator extends StarlingMediator
    {
        override public function onRegister(): void
        {
            const stage: Stage = Starling.current.nativeStage as Stage;
            stage.getChildAt(0).loaderInfo.uncaughtErrorEvents.addEventListener(
                    UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler);

            dispatch(new StageResizeEvent(StageResizeEvent.RESIZED, stage.stageWidth, stage.stageHeight));

            Starling.current.stage.addEventListener(ResizeEvent.RESIZE, resizeHandler);
        }

        override public function onRemove(): void
        {
            Starling.current.stage.removeEventListener(ResizeEvent.RESIZE, resizeHandler);

            const stage: Stage = Starling.current.nativeStage as Stage;
            stage.getChildAt(0).loaderInfo.uncaughtErrorEvents.removeEventListener(
                    UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler);
        }

        private function resizeHandler(e: ResizeEvent): void
        {
            dispatch(new StageResizeEvent(StageResizeEvent.RESIZED, e.width, e.height));

            Starling.current.stage.stageWidth = e.width;
            Starling.current.stage.stageHeight = e.height;
            Starling.current.viewPort = new Rectangle(0, 0, e.width, e.height);
        }

        private function uncaughtErrorHandler(event: UncaughtErrorEvent): void
        {
            event.preventDefault();
            var message: String;
            switch (true) {
                case event.error is Error:
                    const e: Error = event.error as Error;
                    message = e.message;
                    if (Capabilities.isDebugger)
                        message += "\n" + e.getStackTrace();
                    break;

                case event.error is ErrorEvent:
                    message = ErrorEvent(event.error).text;
                    break;

                default:
                    message = event.error.toString();
                    break;
            }
            dispatch(new FatalErrorEvent(FatalErrorEvent.UNCAUGHT_ERROR, message));
        }
    }
}
