package com.ruzzz.csdragons.view.board
{
    import com.ruzzz.csdragons.view.board.events.DragonHiddenEvent;
    import com.ruzzz.lib.starling.SpriteEx;
    import com.ruzzz.lib.starling.shotTexture;

    import flash.display.DisplayObject;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import library.BoardBackground;

    import starling.display.BlendMode;
    import starling.display.Image;
    import starling.display.Quad;
    import starling.display.Sprite;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.text.BitmapFont;
    import starling.text.TextField;
    import starling.textures.Texture;
    import starling.utils.HAlign;
    import starling.utils.VAlign;

    public final class BoardScreen extends SpriteEx
    {
        private var _background: Image;
        private var _dragons: Vector.<Dragon>;
        private var _textField: TextField;
        private var _holderText: Sprite;

        public function BoardScreen()
        {
            super();
            touchable = true;
            _dragons = new Vector.<Dragon>();

            TextField.registerBitmapFont(new BitmapFont);
        }

        override protected function init(): void
        {
            if (!hasEventListener(TouchEvent.TOUCH))
                addEventListener(TouchEvent.TOUCH, touchHandler);

            var bgInstance: DisplayObject = new BoardBackground();
            var bgSize: Rectangle = bgInstance.getBounds(bgInstance);
            var texture: Texture = shotTexture(bgInstance,
                    new Point((width + 2) / bgSize.width, (height + 2) / bgSize.height),
                    bgSize).texture;
            if (_background && contains(_background))
                removeChild(_background, true);
            _background = new Image(texture);
            _background.touchable = true;
            addChildAt(_background, 0);

            _holderText = new Sprite();
            _holderText.blendMode = BlendMode.NONE;
            var bgText: Quad = new Quad(50, 9, 0x0, false);
            bgText.scaleX = bgText.scaleY = 2;
            bgText.y = 48;
            _holderText.addChild(bgText);

            _textField = new TextField(48, 9, "", BitmapFont.MINI, BitmapFont.NATIVE_SIZE,
                    0xffffff);
            _textField.scaleX = _textField.scaleY = 2;
            _textField.x = bgText.x + 3;
            _textField.y = bgText.y;
            _textField.hAlign = HAlign.LEFT;
            _textField.vAlign = VAlign.TOP;
            _textField.touchable = false;
            _holderText.addChild(_textField);
            addChild(_holderText);

            for (var i: int = 0; i < 20; ++i)
                addDragon(new Point(Math.round(Math.random() * width), Math.round(Math.random() * height)));
        }

        override protected function freeUp(): void
        {
            removeEventListener(TouchEvent.TOUCH, touchHandler);
            _background = null;
            removeAllChildren(true);
        }

        private function addDragon(pos: Point): void
        {
            var dragon: Dragon = new Dragon();
            dragon.x = Math.round(pos.x);
            dragon.y = Math.round(pos.y);
            addChild(dragon);
            _dragons.push(dragon);
            dragon.addEventListener(DragonHiddenEvent.HIDDEN, dragonHiddenHandler);

            _textField.text = "Count: " + _dragons.length.toString();
            addChild(_holderText);
        }

        private function removeDragon(dragon: Dragon): void
        {
            _dragons.splice(_dragons.indexOf(dragon), 1);

            _textField.text = "Count: " + _dragons.length.toString();
            addChild(_holderText);
        }

        private function touchHandler(e: TouchEvent): void
        {
            var touch: Touch = e.getTouch(this);
            if (touch) {
                switch (touch.phase) {
                    case TouchPhase.ENDED:
                        var pos: Point = touch.getLocation(this);
                        addDragon(pos);
                        break;
                }
            }
        }

        private function dragonHiddenHandler(e: DragonHiddenEvent): void
        {
            var dragon: Dragon = e.target as Dragon;
            dragon.removeEventListener(DragonHiddenEvent.HIDDEN, dragonHiddenHandler);
            removeDragon(dragon);
        }
    }
}
