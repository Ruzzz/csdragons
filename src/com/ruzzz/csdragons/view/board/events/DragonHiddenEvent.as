package com.ruzzz.csdragons.view.board.events
{
    import starling.events.Event;

    public class DragonHiddenEvent extends Event
    {
        public static const HIDDEN: String = "dragonHidden";

        public function DragonHiddenEvent(type: String)
        {
            super(type);
        }

        public function clone(): Event
        {
            return new DragonHiddenEvent(type);
        }
    }
}
