package com.ruzzz.csdragons.view.board
{
    import com.ruzzz.csdragons.view.board.events.DragonHiddenEvent;
    import com.ruzzz.csdragons.view.skin.DragonSkin;
    import com.ruzzz.lib.starling.SpriteEx;
    import com.ruzzz.lib.starling.TexturesData;

    import flash.geom.Point;
    import flash.geom.Rectangle;

    import starling.core.Starling;
    import starling.display.DisplayObject;
    import starling.display.MovieClip;
    import starling.events.Event;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.textures.Texture;

    [Event(name="dragonHidden", type="starling.events.Event")]
    public class Dragon extends SpriteEx
    {
        static public var skin: DragonSkin;  // TODO: Replace to common skin class

        private var mc_: MovieClip;
        private var active_: Boolean;
        private var state_: String;

        public function Dragon()
        {
            super();
            touchable = true;
            touchGroup = true;
            active_ = false;
            state_ = DragonSkin.SHOW;
        }

        override protected function init(): void
        {
            if (!hasEventListener(TouchEvent.TOUCH))
                addEventListener(TouchEvent.TOUCH, touchHandler);

            updateSkin();
        }

        override protected function freeUp(): void
        {
            removeEventListener(TouchEvent.TOUCH, touchHandler);
            removeAllChildren(true);
        }

        public override function hitTest(pt: Point, forTouch: Boolean = false): DisplayObject
        {
            if (!forTouch)
                return null;
            var bounds: Rectangle = mc_.getBounds(this);
            return bounds.containsPoint(pt) ? this : null;
        }

        public function updateSkin(): void
        {
            if (!skin)
                return;

            cleanUpCurrentAnimation();

            var d: TexturesData = skin.getTextures(state_);
            var textures: Vector.<Texture> = d.textures;
            mc_ = new MovieClip(textures, 24);  // FPS from assets file

            if (state_ != DragonSkin.NORMAL)
                mc_.addEventListener(Event.COMPLETE, mcAnimationCompleteHandler);
            else
                active_ = true;

            d.applyOffset(mc_);
            mc_.touchable = false;

            addChild(mc_);

            mc_.play();
            Starling.juggler.add(mc_);
        }

        private function cleanUpCurrentAnimation(): void
        {
            if (mc_) {
                removeChild(mc_);
                Starling.juggler.remove(mc_);
                mc_.stop();
                mc_ = null;
            }
        }

        private function mcAnimationCompleteHandler(e: Event): void
        {
            (e.target as DisplayObject).removeEventListeners(Event.COMPLETE);

            switch (state_) {
                case DragonSkin.HIDE:
                    cleanUpCurrentAnimation();
                    dispatchEvent(new DragonHiddenEvent(DragonHiddenEvent.HIDDEN));
                    removeFromParent(true);
                    break;

                case DragonSkin.SHOW:
                    state_ = DragonSkin.NORMAL;
                    updateSkin();
            }
        }

        private function touchHandler(e: TouchEvent): void
        {
            var touch: Touch = e.getTouch(this);
            if (touch) {
                switch (touch.phase) {
                    case TouchPhase.ENDED:
                        e.stopImmediatePropagation();
                        if (active_) {
                            active_ = false;
                            state_ = DragonSkin.HIDE;
                            updateSkin();
                        }
                        break;
                }
            }
        }
    }
}
