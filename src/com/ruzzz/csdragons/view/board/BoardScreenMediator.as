package com.ruzzz.csdragons.view.board
{
    import org.robotlegs.mvcs.StarlingMediator;

    public final class BoardScreenMediator extends StarlingMediator
    {
        [Inject] public var boardScreen: BoardScreen;

        override public function onRegister(): void
        {
            // TODO
        }

        override public function onRemove(): void
        {
            // TODO
        }
    }
}
