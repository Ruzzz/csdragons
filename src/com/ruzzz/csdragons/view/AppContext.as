package com.ruzzz.csdragons.view
{
    import com.ruzzz.csdragons.controller.FatalErrorCommand;
    import com.ruzzz.csdragons.controller.InitAppCommand;
    import com.ruzzz.csdragons.controller.ScreenCommand;
    import com.ruzzz.csdragons.controller.StageResizeCommand;
    import com.ruzzz.csdragons.model.AppModel;
    import com.ruzzz.csdragons.model.events.FatalErrorEvent;
    import com.ruzzz.csdragons.model.events.ScreenEvent;
    import com.ruzzz.csdragons.model.events.StageResizeEvent;
    import com.ruzzz.csdragons.view.board.BoardScreen;
    import com.ruzzz.csdragons.view.board.BoardScreenMediator;

    import org.robotlegs.base.ContextEvent;
    import org.robotlegs.mvcs.StarlingContext;

    import starling.display.DisplayObjectContainer;
    import starling.display.Stage;

    public class AppContext extends StarlingContext
    {
        public function AppContext(contextView: DisplayObjectContainer)
        {
            super(contextView);
        }

        override public function startup(): void
        {
            injector.mapSingleton(AppModel);

            commandMap.mapEvent(StageResizeEvent.RESIZED, StageResizeCommand, StageResizeEvent);
            commandMap.mapEvent(FatalErrorEvent.UNCAUGHT_ERROR, FatalErrorCommand, FatalErrorEvent);
            commandMap.mapEvent(FatalErrorEvent.FATAL_ERROR, FatalErrorCommand, FatalErrorEvent);

            commandMap.mapEvent(ScreenEvent.SHOW, ScreenCommand, ScreenEvent);
            commandMap.mapEvent(ScreenEvent.PREVIOUS, ScreenCommand, ScreenEvent);

            mediatorMap.mapView(BoardScreen, BoardScreenMediator);

            mediatorMap.mapView(Stage, StageMediator);
            mediatorMap.createMediator(contextView.stage);

            commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, InitAppCommand, ContextEvent);
            super.startup();
        }
    }
}
