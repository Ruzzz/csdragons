package com.ruzzz.csdragons.view
{
    import starling.display.Sprite;

    public final class AppSprite extends Sprite
    {
        private var context_: AppContext;

        public function AppSprite()
        {
            super();
            context_ = new AppContext(this);
        }
    }
}
