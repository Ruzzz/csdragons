package com.ruzzz.csdragons.controller
{
    import com.ruzzz.csdragons.model.AppModel;
    import com.ruzzz.csdragons.model.events.StageResizeEvent;
    import com.ruzzz.csdragons.view.AppConsts;
    import com.ruzzz.csdragons.view.board.Dragon;
    import com.ruzzz.csdragons.view.skin.DragonSkin;

    import org.robotlegs.mvcs.StarlingCommand;

    import starling.display.DisplayObject;

    public final class StageResizeCommand extends StarlingCommand
    {
        [Inject] public var event: StageResizeEvent;
        [Inject] public var appModel: AppModel;

        override public function execute(): void
        {
            appModel.width = event.width;
            appModel.height = event.height;

            if (event.width > 0) {
                // Re-render all skin
                // TODO: Magic number
                // TODO: Use height for landscape
                Dragon.skin = new DragonSkin(Math.round(event.width / DragonSkin.WIDTH_DIV));
            }

            var currentScreen: DisplayObject = contextView.getChildByName(AppConsts.SCREEN_LAYER_NAME);
            if (currentScreen) {
                currentScreen.width = appModel.width;
                currentScreen.height = appModel.height;
            }
        }
    }
}
