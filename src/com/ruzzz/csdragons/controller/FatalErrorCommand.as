package com.ruzzz.csdragons.controller
{
    import com.ruzzz.csdragons.model.events.FatalErrorEvent;

    public class FatalErrorCommand
    {
        [Inject] public var event: FatalErrorEvent;

        public function execute():void
        {
            // TODO: Send stats or/and show msg
            trace(event.message);
        }
    }
}
