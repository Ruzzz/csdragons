package com.ruzzz.csdragons.controller
{
	import com.ruzzz.csdragons.model.consts.ScreenName;
	import com.ruzzz.csdragons.model.events.ScreenEvent;

	import org.robotlegs.mvcs.StarlingCommand;

	public final class InitAppCommand extends StarlingCommand
	{
		override public function execute(): void
		{
			// TODO: Init app, load config
			dispatch(new ScreenEvent(ScreenEvent.SHOW, ScreenName.BOARD));
		}
	}
}
