package com.ruzzz.csdragons.controller
{
    import com.ruzzz.csdragons.model.AppModel;
    import com.ruzzz.csdragons.model.consts.ScreenName;
    import com.ruzzz.csdragons.model.events.ScreenEvent;
    import com.ruzzz.csdragons.view.AppConsts;
    import com.ruzzz.csdragons.view.board.BoardScreen;

    import org.robotlegs.mvcs.StarlingCommand;

    import starling.display.DisplayObject;
    import starling.display.DisplayObjectContainer;

    public final class ScreenCommand extends StarlingCommand
    {
        [Inject] public var event: ScreenEvent;
        [Inject] public var appModel: AppModel;

        override public function execute(): void
        {
            switch (event.type) {
                case ScreenEvent.SHOW:
                    onShow();
                    break;

                case ScreenEvent.PREVIOUS:
                    // TODO:
                    break;
            }
        }

        private function onShow(): void
        {
            var screenName: String = event.name;
            if (appModel.currentScreen == screenName)
                return;

            var newScreen: DisplayObjectContainer;
            switch (screenName) {
                case ScreenName.BOARD:
                    newScreen = new BoardScreen;
                    break;
                default:
                    throw new ArgumentError("Invalid ScreenEvent.name");
            }

            setCurrentScreen(newScreen);
            appModel.currentScreen = screenName;
        }

        private function setCurrentScreen(value: DisplayObject): void
        {
            var currentScreen: DisplayObject = contextView.getChildByName(AppConsts.SCREEN_LAYER_NAME);
            if (currentScreen)
                contextView.removeChild(currentScreen, true);
            if (value) {
                value.name = AppConsts.SCREEN_LAYER_NAME;
                value.width = appModel.width;
                value.height = appModel.height;
                contextView.addChild(value);
            }
        }
    }
}
